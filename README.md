# WeatherFrontend
This project goal is to get the current weather situation displaying the city name plus average temperature and the
wind strength. Clicking on an item shows the forecast in the next hours.

# Development
1. Clone the project (https://github.com/sajanv88/weather-frontend.git)
2. Run ``` npm install ``` in cloned directory
3. Run ``` npm start ``` for a dev server and navigate to (http://localhost:4200)
4. Run ```npm run test``` to execute the unit tests via [Karma](https://karma-runner.github.io).

# Release
1. ``` npm run build:prod ``` The build artifacts will be stored in the `dist/` directory. use it for a production.
2. ``` npm run server ``` Navigate to (http://localhost:8080)

# Important File Structure
```
.
├── dist # production release build lives here
├── e2e # end to end testing lives here.
├── src
│   ├── app
│   │   └── modules   # Application source code lives here.
│   ├── assests
│   │   └── europen_city.json  # Contain 5 europen city data
│   └── environments # Important endpoint configuration located
└── ng-toolkit.json # generated from ng-toolkit. Visit (https://www.npmjs.com/package/@ng-toolkit/universal)

```

# A note for whosoever alternatively `Reviewer`
  This application works as expected also, I have added a normal list view for weather forcast but you can toggle view between
  chart and normal list.  I would like to mention I have used server side rendering for better performance and SEO. 

  since, I am going to India for vacation I didn't have enough time that said, I have not covered 100% unit test but to prove my skill I have covered enough tests. Also, I am happy to explain if you any questions. 
