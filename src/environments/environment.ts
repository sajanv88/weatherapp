// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  citiesApiEndpoint: '/assets/europen_city.json',
  openWeatherAppId: 'fb01af38e440be0cac8de80a2fc9fbc1',
  openWeatherApiEndpoint: 'http://api.openweathermap.org/data/2.5/weather?units=metric',
  openWeatherForeCastApiEndpoint: 'http://api.openweathermap.org/data/2.5/forecast?units=metric'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
