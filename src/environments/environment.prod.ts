export const environment = {
  production: true,
  citiesApiEndpoint: '/assets/europen_city.json',
  openWeatherAppId: 'fb01af38e440be0cac8de80a2fc9fbc1',
  openWeatherApiEndpoint: 'http://api.openweathermap.org/data/2.5/weather?units=metric',
  openWeatherForeCastApiEndpoint: 'http://api.openweathermap.org/data/2.5/forecast?units=metric'
};
