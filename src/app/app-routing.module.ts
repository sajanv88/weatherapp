import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GridComponent } from './modules/grid/grid.component';
import { WeatherForecastComponent } from './modules/weather-forecast/weather-forecast.component';

const routes: Routes = [{
  path: '',
  component: GridComponent,
  pathMatch: 'full'
}, {
  path: 'forecast/:city/:cityid',
  component: WeatherForecastComponent,
  pathMatch: 'full'
}];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
