import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './modules/services/api.service.service';
import { GridModule } from './modules/grid/grid.module';
import { WeatherForecastModule } from './modules/weather-forecast/weather-forecast.module';
import { ErrorModule } from './modules/error/error.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    CommonModule,
    NgtUniversalModule,
    TransferHttpCacheModule,
    HttpClientModule,
    AppRoutingModule,
    GridModule,
    WeatherForecastModule,
    ErrorModule
  ],
  providers: [ApiService],
})
export class AppModule { }
