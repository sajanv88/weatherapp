import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { ErrorComponent } from './modules/error/error.component';
import { HttpClientModule, HttpClient, HttpHandler } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { GridModule } from './modules/grid/grid.module';
import { WeatherForecastModule } from './modules/weather-forecast/weather-forecast.module';
import { ErrorModule } from './modules/error/error.module';
import { ApiService } from './modules/services/api.service.service';
import { Observable, of, Subject } from 'rxjs';
import { ICity } from './modules/weather-card/city.interface';
import { IReport } from './modules/weather-card/weather.interface';
import { ForeCast } from './modules/weather-forecast/model/weather-forcast.model';

const mockCities: ICity = {
  id: 6539761,
  name: 'Rome',
  country: 'IT',
  coord: {
    lon: 12.64,
    lat: 41.76
  },
  image: 'https://res.cloudinary.com/dbzuuz1fm/image/upload/v1553199131/rome.jpg'
};

describe('AppComponent', () => {
});
