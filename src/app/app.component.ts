import { Component, OnInit } from '@angular/core';
import { ApiService } from './modules/services/api.service.service';
import {
  Subscription,
  merge
} from 'rxjs';
import { ErrorInfo } from './modules/error/error.component';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'w-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'weather-frontend';
  public error: ErrorInfo;
  constructor(private api: ApiService) {}

  ngOnInit() {
    const sub: Subscription  = this.api.exception
    .pipe(merge)
    .subscribe((val: HttpErrorResponse) => {
      if (!val.ok) {
        this.error = {
          type: 'danger',
          message: 'Unable to load weather report. Something went wrong!'
        };
      }
    });
  }
}
