import {
  ITemperature,
  IWeather,
  IWind
} from '../../weather-card/weather.interface';

export class WeatherInfo {

  private $windInfo: IWind;
  private $temperature: ITemperature;
  private $weatherInfo: IWeather[];

  set wind(wind: IWind) {
    this.$windInfo = wind;
  }

  get wind(): IWind {
    return this.$windInfo;
  }

  set temperature(temp: ITemperature) {
    this.$temperature = temp;
  }

  get temperature(): ITemperature {
    return this.$temperature;
  }

  set weather(weather: IWeather[]) {
    this.$weatherInfo = weather;
  }

  get weather(): IWeather[] {
    return this.$weatherInfo;
  }
}
