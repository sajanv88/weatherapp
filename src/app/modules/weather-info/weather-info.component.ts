import {
  Component,
  OnInit,
  Input,
  OnDestroy
} from '@angular/core';
import { WeatherInfo } from './model/weather-info.model';
import {
  IWind,
  ITemperature,
  IWeather
} from '../weather-card/weather.interface';

@Component({
  selector: 'w-weather-info',
  templateUrl: './weather-info.component.html',
  styleUrls: ['./weather-info.component.sass']
})
export class WeatherInfoComponent implements OnDestroy {
  public info: WeatherInfo = new WeatherInfo();

  @Input()
  public set wind(wind: IWind) {
    this.info.wind = wind;
  }

  @Input()
  public set temperature(temp: ITemperature) {
    this.info.temperature = temp;
  }

  @Input()
  public set weather(weather: IWeather[]) {
    this.info.weather = weather;
  }

  constructor() { }

  /**
   * Gets weather icon
   * '@param' icon
   * @returns weather icon
   */
  public getWeatherIcon(icon: string): string {
    return `http://openweathermap.org/img/w/${icon}.png`;
  }

  ngOnDestroy() {
    if (this.info) {
      this.info = null;
    }
  }
}
