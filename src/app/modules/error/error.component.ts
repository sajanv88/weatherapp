import { Component, OnInit, Input } from '@angular/core';

export interface ErrorInfo {
  type: 'danger' | 'info' | 'success' | 'warning';
  message: string;
}

@Component({
  selector: 'w-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.sass']
})
export class ErrorComponent implements OnInit {

  @Input()
  public isError: boolean;

  @Input()
  public errorInfo: ErrorInfo;

  constructor() { }

  ngOnInit() {
    this.isError = this.isError || false;
  }

  public onClose(): void {
    this.isError = false;
    this.errorInfo = null;
  }
}
