import { ErrorComponent } from './error.component';
describe('ErrorComponent behaviour', () => {
  const component: ErrorComponent = new ErrorComponent();
  describe('No Error', () => {
    it('should not have visible when there is no error', async () => {
      await component.ngOnInit();
      await expect(component.isError).toBe(false);
    });
  });
  describe('Caught Error', () => {
    beforeEach(() => {
      component.isError = true;
      component.errorInfo = {
        type: 'danger',
        message: 'Caught Error'
      };
    });
    it('should visible when there is an error', async () => {
      await component.ngOnInit();
      await expect(component.isError).toBe(true);
      await expect(component.errorInfo).toBeTruthy();
      await expect(component.errorInfo.type).toBe('danger');
      await expect(component.errorInfo.message).toBe('Caught Error');
    });
    it('should hide the error box when user clicked close button', async () => {
      await component.onClose();
      await expect(component.isError).toBe(false);
      await expect(component.errorInfo).toBeFalsy();
    });
  });
});
