import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'w-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.sass']
})
export class SwitchComponent implements OnInit {

  public checked: boolean;

  /**
   * Output of switch component
   */
  @Output()
  public switchHandler: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit() {
    this.checked = this.checked || false;
  }

  /**
   * Determines whether switch changed on
   */
  public onSwitchChanged(): void {
    this.switchHandler.emit(this.checked);
  }

}
