import { SwitchComponent } from './switch.component';
describe('SwitchComponent behaviour', () => {
  const component: SwitchComponent = new SwitchComponent();
  beforeEach(() => {
    component.ngOnInit();
  });
  it('should have default value false aka when it is "off" ', async () => {
    await expect(component.checked).toBe(false);
  });
  it('should have value true aka when it is "on" ', async () => {
    component.onSwitchChanged();
    component.switchHandler.subscribe(async (val: boolean) => {
      await expect(component.checked).toBe(true);
    });
  });
});
