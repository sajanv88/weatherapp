import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpErrorResponse
} from '@angular/common/http';
import {
  Observable,
  Subject,
  of
} from 'rxjs';
import {
  Cites,
  ICity
} from '../weather-card/city.interface';
import {
  map,
  catchError
} from 'rxjs/operators';
import { IReport } from '../weather-card/weather.interface';
import { environment } from '../../../environments/environment';
import { ForeCast } from '../weather-forecast/model/weather-forcast.model';
import { IForeCast } from '../weather-forecast/forecast-list.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  /**
   * Exception handling of api service
   */
  public exception: Subject<HttpErrorResponse> = new Subject<HttpErrorResponse>();
  constructor(private $http: HttpClient) { }

  /**
   * Gets cities
   * @returns cities
   */
  public getCities(): Observable<ICity[]> {
    const endPoint = environment.citiesApiEndpoint;
    return this.$http.get(endPoint).pipe(
      map( (cities: Cites) => cities.cites)
    );
  }

  /**
   * Fetchs weather by city
   * '@param' cityId
   * @returns weather by city
   */
  public fetchWeatherByCity(cityId: number): Observable<IReport> {
    const endPoint = `${environment.openWeatherApiEndpoint}&id=${cityId}&appid=${environment.openWeatherAppId}`;
    return this.$http.get(endPoint).pipe(
      catchError(err => of(this.exception.next(err))),
      map((weatherReport: IReport) => weatherReport)
    );
  }

  /**
   * Fetchs fore cast by city
   * '@param' cityId
   * @returns forecast array by city
   */
  public fetchForeCastByCity(cityId: number): Observable<ForeCast[]> {
    const endPoint = `${environment.openWeatherForeCastApiEndpoint}&id=${cityId}&appid=${environment.openWeatherAppId}`;
    return this.$http.get(endPoint).pipe(
      catchError(err => of(this.exception.next(err))),
      map((foreCast: IForeCast) => foreCast.list.map(item => {
        const foreCastInfo: ForeCast = new ForeCast(item.dt_txt, item.dt);
        foreCastInfo.temperature = item.main;
        foreCastInfo.weather = item.weather;
        foreCastInfo.wind = item.wind;
        return foreCastInfo;
      }))
    );
  }
}
