import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ApiService } from './api.service.service';
import { ICity } from '../weather-card/city.interface';
import { IReport } from '../weather-card/weather.interface';
import { environment } from 'src/environments/environment';
import { ForeCast } from '../weather-forecast/model/weather-forcast.model';

const mockCitisRes = {
  cites: [{
    id: 1,
    name: 'Rome',
    coord: {
      lat: 29,
      lon: 40
    },
    country: 'IT',
   image: 'http://imagepath.com'
  }, {
    id: 2,
    name: 'Amsterdam',
    coord: {
      lat: 239,
      lon: 420
    },
    country: 'NL',
    image: 'http://imagepath.com'
  }]
};

const mockWeatherInfoRes = {
  weather: [{
    id: 801,
    main: 'Clouds',
    description: 'few clouds',
    icon: '02d'
  }],
  main: {
    temp: 8.44,
    pressure: 1032,
    humidity: 65,
    temp_min: 7,
    temp_max: 9.44
  },
  wind: {
    speed: 8.2,
    deg: 260
  }
};

const mockForecastRes = {
  list: [{
    dt_txt: '2019-03-24 12:00:00',
    weather: [{
      id: 801,
      main: 'Clouds',
      description: 'few clouds',
      icon: '02d'
    }],
    main: {
      temp: 8.44,
      pressure: 1032,
      humidity: 65,
      temp_min: 7,
      temp_max: 9.44
    },
    wind: {
      speed: 8.2,
      deg: 260
    }
  }, {
    dt_txt: '2019-03-24 15:00:00',
    weather: [{
      id: 901,
      main: 'Rain',
      description: 'Rainy',
      icon: '02d'
    }],
    main: {
      temp: 3.44,
      pressure: 1032,
      humidity: 65,
      temp_min: 7,
      temp_max: 9.44
    },
    wind: {
      speed: 20.2,
      deg: 260
    }
  }]
};

describe('ApiService', () => {
  let api: ApiService;
  let httpClientMock: HttpTestingController;
  beforeEach(() => {
      TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService]
    });
      api = TestBed.get(ApiService);
      httpClientMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpClientMock.verify();
  });

  it('should be created', () => {
    expect(api).toBeTruthy();
  });

  it('should fetch cities', () => {
    api.getCities().subscribe((cities: ICity[]) => {
      expect(cities.length).toBe(2);
    });
    const endPoint = environment.citiesApiEndpoint;
    const req = httpClientMock.expectOne(endPoint);
    expect(req.request.method).toBe('GET');
    req.flush(mockCitisRes);
  });

  it('should fetch weather information by passing city id', () => {
    api.fetchWeatherByCity(1).subscribe((weatherReport: IReport) => {
      expect(weatherReport.main.temp).toBe(8.44);
      expect(weatherReport.wind.speed).toBe(8.2);
      expect(weatherReport.weather.length).toBe(1);
      expect(weatherReport.weather[0].description).toBe('few clouds');

    });
    const endPoint = `${environment.openWeatherApiEndpoint}&id=1&appid=${environment.openWeatherAppId}`;
    const req = httpClientMock.expectOne(endPoint);
    expect(req.request.method).toBe('GET');
    req.flush(mockWeatherInfoRes);
  });

  it('should fetch weather forecast for next few days and hours by passing city id', () => {
    api.fetchForeCastByCity(1).subscribe((foreCast: ForeCast[]) => {
      expect(foreCast.length).toBe(2);
    });
    const endPoint = `${environment.openWeatherForeCastApiEndpoint}&id=1&appid=${environment.openWeatherAppId}`;
    const req = httpClientMock.expectOne(endPoint);
    expect(req.request.method).toBe('GET');
    req.flush(mockForecastRes);
  });
});
