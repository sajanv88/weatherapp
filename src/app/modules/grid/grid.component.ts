import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service.service';
import { Subscription } from 'rxjs';
import { ICity } from '../weather-card/city.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'w-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.sass']
})
export class GridComponent implements OnInit {
  public cities: Array<ICity>;
  constructor(
    public api: ApiService,
    private router: Router) { }

  ngOnInit() {
    const citySubscription: Subscription = this.api.getCities()
    .subscribe((cities: ICity[]) => {
        this.cities = cities;
        citySubscription.unsubscribe(); // close the subscriber
      }
    );
  }

  /**
   * navigate to forecast
   * '@param' city
   */
  public navigateToForecast(city: ICity): void {
    this.router.navigate(['/forecast', city.name, city.id]);
  }
}
