import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridComponent } from './grid.component';
import { WeatherCardModule } from '../weather-card/weather-card.module';
import { ApiService } from '../services/api.service.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('GridComponent', () => {
  let component: GridComponent;
  let fixture: ComponentFixture<GridComponent>;
  let api: ApiService;
  let httpClient: HttpTestingController;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridComponent ],
      imports: [ RouterTestingModule, HttpClientTestingModule, WeatherCardModule ],
      providers: [ApiService]
    })
    .compileComponents();
    api = TestBed.get(ApiService);
    httpClient = TestBed.get(HttpTestingController);
  }));

  beforeEach(async () => {
    fixture = TestBed.createComponent(GridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
