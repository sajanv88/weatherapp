import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridComponent } from './grid.component';
import { WeatherCardModule } from '../weather-card/weather-card.module';

@NgModule({
  declarations: [GridComponent],
  imports: [
    CommonModule,
    WeatherCardModule
  ]
})
export class GridModule { }
