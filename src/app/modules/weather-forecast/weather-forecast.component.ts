import {
  Component,
  OnInit
} from '@angular/core';
import {
  Router,
  ActivatedRoute
} from '@angular/router';
import { ApiService } from '../services/api.service.service';
import { Subscription } from 'rxjs';
import { ForeCast } from './model/weather-forcast.model';
import { mergeMap } from 'rxjs/operators';

@Component({
  selector: 'w-weather-forecast',
  templateUrl: './weather-forecast.component.html',
  styleUrls: ['./weather-forecast.component.sass']
})
export class WeatherForecastComponent implements OnInit {

  public cityName: string;
  public weatherForecast: Array<ForeCast>;
  public isChartView: boolean;

  constructor(
    private api: ApiService,
    private activeRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    const activeRouteSub: Subscription = this.activeRoute.paramMap
    .pipe(
      mergeMap(params => {
        this.cityName = params.get('city');
        return this.api.fetchForeCastByCity(Number(params.get('cityid')));
      })
    )
    .subscribe((foreCast: ForeCast[]) => {
      this.weatherForecast = foreCast;
      activeRouteSub.unsubscribe();
    });
    this.isChartView = this.isChartView || false;
  }

  /**
   * Go back
   */
  public goBack(): void {
    this.router.navigate(['']);
  }

  /**
   * Toggles view
   * '@param' event
   */
  public toggleView(isValue: boolean): void {
    this.isChartView = isValue;
  }
}
