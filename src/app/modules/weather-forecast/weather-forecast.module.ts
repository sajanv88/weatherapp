import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherForecastComponent } from './weather-forecast.component';
import { WeatherInfoModule } from '../weather-info/weather-info.module';
import { WeatherChartModule } from '../weather-chart/weather-chart.module';
import { SwitchModule } from '../switch/switch.module';

@NgModule({
  declarations: [WeatherForecastComponent],
  imports: [
    CommonModule,
    WeatherInfoModule,
    WeatherChartModule,
    SwitchModule
  ],
  exports: [WeatherForecastComponent]
})
export class WeatherForecastModule { }
