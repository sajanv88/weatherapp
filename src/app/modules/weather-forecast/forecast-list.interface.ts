import {
  ITemperature,
  IWeather,
  IWind
} from '../weather-card/weather.interface';

export interface IForeCastList {
  dt: number;
  main: ITemperature;
  weather: IWeather[];
  wind: IWind;
  dt_txt: string;
}

export interface IForeCast {
  list: Array<IForeCastList>;
}
