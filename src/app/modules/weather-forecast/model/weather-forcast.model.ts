import { Weather } from '../../weather-card/model/weather.model';

export class ForeCast extends Weather {

  private readonly $dateAndTime: string;
  private readonly $date: number;

  constructor(dateAndTime: string, date: number) {
    super();
    this.$dateAndTime = dateAndTime;
    this.$date = date;
  }

  get dateAndTime(): string {
    return this.$dateAndTime;
  }

  get date(): number {
    return this.$date;
  }
}
