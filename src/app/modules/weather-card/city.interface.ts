export interface Coordinate {
  lon: number;
  lat: number;
}

export interface ICity {
  id: number;
  name: string;
  country: string;
  coord: Coordinate;
  image: string;
}

export interface Cites {
  cites: Array<ICity>;
}
