import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherCardComponent } from './weather-card.component';
import { CommonModule } from '@angular/common';
import { WeatherInfoModule } from '../weather-info/weather-info.module';
import { RouterTestingModule } from '@angular/router/testing';
import { ApiService } from '../services/api.service.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { ICity } from './city.interface';
import { City } from './model/weather.model';

const mockCityInfo: ICity = {
  id: 1,
  name: 'Rome',
  coord: {
    lat: 29,
    lon: 40
  },
  country: 'IT',
 image: 'http://imagepath.com'
};

describe('WeatherCardComponent', () => {
  let component: WeatherCardComponent;
  let fixture: ComponentFixture<WeatherCardComponent>;
  let api: ApiService;
  let httpClient: HttpTestingController;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherCardComponent ],
      imports: [CommonModule, RouterTestingModule, HttpClientTestingModule, WeatherInfoModule],
      providers: [ApiService]
    })
    .compileComponents();
    api = TestBed.get(ApiService);
    httpClient = TestBed.get(HttpTestingController);
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherCardComponent);
    component = fixture.componentInstance;
    component.cityInfo = new City(
      mockCityInfo.id,
      mockCityInfo.name,
      mockCityInfo.country,
      mockCityInfo.coord,
      mockCityInfo.image
    );
    component.api.fetchWeatherByCity(1);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
