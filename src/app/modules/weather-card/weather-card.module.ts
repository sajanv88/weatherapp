import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherCardComponent } from './weather-card.component';
import { WeatherInfoModule } from '../weather-info/weather-info.module';

@NgModule({
  declarations: [WeatherCardComponent],
  exports: [WeatherCardComponent],
  imports: [
    CommonModule,
    WeatherInfoModule
  ]
})
export class WeatherCardModule { }
