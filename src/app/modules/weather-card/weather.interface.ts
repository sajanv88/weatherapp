export interface IWind {
  speed: number;
  deg: number;
}

export interface ITemperature {
  temp: number;
  pressure: number;
  humidity: number;
}

export interface IWeather {
  main: string;
  icon: string;
  description: string;
}

export interface IReport {
  wind: IWind;
  weather: IWeather[];
  main: ITemperature;
}
