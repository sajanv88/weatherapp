import { Coordinate } from '../city.interface';
import {
  ITemperature,
  IWeather,
  IWind
} from '../weather.interface';


export class Weather {

  private $windInfo: IWind;
  private $temperature: ITemperature;
  private $weatherInfo: IWeather[];

  set wind(wind: IWind) {
    this.$windInfo = wind;
  }

  get wind(): IWind {
    return this.$windInfo;
  }

  set temperature(temp: ITemperature) {
    this.$temperature = temp;
  }

  get temperature(): ITemperature {
    return this.$temperature;
  }

  set weather(weather: IWeather[]) {
    this.$weatherInfo = weather;
  }

  get weather(): IWeather[] {
    return this.$weatherInfo;
  }
}

export class City extends Weather {

  private readonly $id: number;
  private readonly $name: string;
  private readonly $country: string;
  private readonly $coordinate: Coordinate;
  private readonly $image: string;

  constructor(
    id: number,
    name: string,
    country: string,
    coord: Coordinate,
    image: string) {
    super();
    this.$id = id;
    this.$name = name;
    this.$country = country;
    this.$coordinate = coord;
    this.$image = image;
  }

  get id(): number {
    return this.$id;
  }

  get name(): string {
    return this.$name;
  }

  get country(): string {
    return this.$country;
  }

  get coord(): Coordinate {
    return this.$coordinate;
  }

  get image(): string {
    return this.$image;
  }

}
