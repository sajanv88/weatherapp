import {
  Component,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { ICity } from './city.interface';
import { City } from './model/weather.model';
import { Subscription } from 'rxjs';
import { ApiService } from '../services/api.service.service';
import { IReport } from './weather.interface';

@Component({
  selector: 'w-weather-card',
  templateUrl: './weather-card.component.html',
  styleUrls: ['./weather-card.component.sass']
})
export class WeatherCardComponent {

  public cityInfo: City;

  /**
   * Sets input
   */
  @Input()
  public set city(city: ICity) {
    this.cityInfo = new City(city.id, city.name, city.country, city.coord, city.image);
    const sub: Subscription = this.api.fetchWeatherByCity(city.id)
      .subscribe((wReport: IReport) => {
        this.cityInfo.wind = wReport.wind;
        this.cityInfo.temperature = wReport.main;
        this.cityInfo.weather = wReport.weather;
        sub.unsubscribe(); // close the subcriber to avoid memory leak
      });
  }

  /**
   * Output  of weather card component
   */
  @Output()
  public actionHandler: EventEmitter<City> = new EventEmitter<City>();

  constructor(public api: ApiService) { }

  /**
   * Invokes weather card component
   */
  public invoke(): void {
    this.actionHandler.emit(this.cityInfo);
  }
}
