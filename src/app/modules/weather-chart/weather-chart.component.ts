import { Component, OnInit, Input } from '@angular/core';
import { Chart } from 'chart.js';
import { ForeCast } from '../weather-forecast/model/weather-forcast.model';

@Component({
  selector: 'w-weather-chart',
  templateUrl: './weather-chart.component.html',
  styleUrls: ['./weather-chart.component.sass']
})
export class WeatherChartComponent implements OnInit {

  @Input()
  public forecast: ForeCast[];

  public chart: Chart;
  constructor() { }

  ngOnInit() {
    const weatherDates = this.forecast.map((forecast: ForeCast) => {
      return new Date(forecast.dateAndTime).toLocaleTimeString('en', {year: 'numeric', month: 'short', day: 'numeric'});
    });
    const temp = this.forecast.map((forecast: ForeCast) => {
      return forecast.temperature.temp;
    });
    const windSpeed = this.forecast.map((forecast: ForeCast) => {
      return forecast.wind.speed;
    });
    this.chart = new Chart('canvas', {
      type: 'line',
      data: {
        labels: weatherDates,
        datasets: [
          {
            label: 'Temperature',
            data: temp,
            borderColor: '#3cba9f',
            fill: false
          },
          {
            label: 'Wind Speed',
            data: windSpeed,
            borderColor: '#ffcc00',
            fill: false
          },
        ]
      },
      options: {
        maintainAspectRatio: true,
        legend: {
          display: true,
        },
        scales: {
          xAxes: [{
            display: true
          }],
          yAxes: [{
            display: true
          }],
        }
      }
    });
  }

}
