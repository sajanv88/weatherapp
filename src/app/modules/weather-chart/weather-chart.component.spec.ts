import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherChartComponent } from './weather-chart.component';
import { CommonModule } from '@angular/common';
import { ForeCast } from '../weather-forecast/model/weather-forcast.model';
import { IForeCast } from '../weather-forecast/forecast-list.interface';

const mockForecastRes = {
  list: [{
    dt: 1553428800,
    dt_txt: '2019-03-24 12:00:00',
    weather: [{
      id: 801,
      main: 'Clouds',
      description: 'few clouds',
      icon: '02d'
    }],
    main: {
      temp: 8.44,
      pressure: 1032,
      humidity: 65,
      temp_min: 7,
      temp_max: 9.44
    },
    wind: {
      speed: 8.2,
      deg: 260
    }
  }, {
    dt: 1553428800,
    dt_txt: '2019-03-24 15:00:00',
    weather: [{
      id: 901,
      main: 'Rain',
      description: 'Rainy',
      icon: '02d'
    }],
    main: {
      temp: 3.44,
      pressure: 1032,
      humidity: 65,
      temp_min: 7,
      temp_max: 9.44
    },
    wind: {
      speed: 20.2,
      deg: 260
    }
  }]
};

const mockWeatherInfoRes = {
  weather: [{
    id: 801,
    main: 'Clouds',
    description: 'few clouds',
    icon: '02d'
  }],
  main: {
    temp: 8.44,
    pressure: 1032,
    humidity: 65,
    temp_min: 7,
    temp_max: 9.44
  },
  wind: {
    speed: 8.2,
    deg: 260
  }
};

describe('WeatherChartComponent', () => {
  let component: WeatherChartComponent;
  let fixture: ComponentFixture<WeatherChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WeatherChartComponent ],
      imports: [ CommonModule ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherChartComponent);
    component = fixture.componentInstance;
    const forecast: ForeCast[] = [];
    mockForecastRes.list.forEach((item) => {
      const foreCastInfo: ForeCast = new ForeCast(item.dt_txt, item.dt);
      foreCastInfo.wind = item.wind;
      foreCastInfo.weather = item.weather;
      foreCastInfo.temperature = item.main;
      forecast.push(foreCastInfo);
    });
    component.forecast = forecast;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
