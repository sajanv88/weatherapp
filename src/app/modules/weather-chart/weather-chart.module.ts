import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WeatherChartComponent } from './weather-chart.component';

@NgModule({
  declarations: [WeatherChartComponent],
  imports: [
    CommonModule
  ],
  exports: [WeatherChartComponent]
})
export class WeatherChartModule { }
